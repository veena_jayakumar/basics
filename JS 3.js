function formValidation() {
    var firstname = document.getElementById('firstname');
    var username = document.getElementById('username');
    var email = document.getElementById('email');
    var phone = document.getElementById('phone');
    // check empty form fields.
    if (firstname.value.length == 0) {
    document.getElementById('message').innerText = "* All fields are mandatory *"; 
    firstname.focus();
    return false;
    }
    // no alert messages.
    if (inputAlphabet(firstname, "* For your name please use alphabets only *")) {
    if (lengthDefine(username, 6, 8)) {
    if (emailValidation(email, "* Not a valid email address *")) {
    if (textNumeric(phone, "* Not a valid Phone number *")) {
    return true;
    }
    }
    }
    }
    return false;
    }
    function textNumeric(inputtext, alertMsg) {
    var numericExpression = /^[0-9]+$/;
    if (inputtext.value.match(numericExpression)) {
    return true;
    } else {
    document.getElementById('p4').innerText = alertMsg; 
    inputtext.focus();
    return false;
    }
    }
    function inputAlphabet(inputtext, alertMsg) {
    var alphaExp = /^[a-zA-Z]+$/;
    if (inputtext.value.match(alphaExp)) {
    return true;
    } else {
    document.getElementById('p1').innerText = alertMsg; 
    alert(alertMsg);
    inputtext.focus();
    return false;
    }
    }
    function lengthDefine(inputtext, min, max) {
    var uInput = inputtext.value;
    if (uInput.length >= min && uInput.length <= max) {
    return true;
    } else {
    document.getElementById('p2').innerText = "* Please enter between " + min + " and " + max + " characters *"; 
    inputtext.focus();
    return false;
    }
    }
    function emailValidation(inputtext, alertMsg) {
    var emailExp = /^[w-.+]+@[a-zA-Z0-9.-]+.[a-zA-z0-9]{2,4}$/;
    if (inputtext.value.match(emailExp)) {
    return true;
    } else {
    document.getElementById('p3').innerText = alertMsg; 
    inputtext.focus();
    return false;
    }
    }